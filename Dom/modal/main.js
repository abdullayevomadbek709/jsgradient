const showBtn = document.getElementById("show-btn");
const closeBtn = document.getElementById("close-btn");
const modal = document.getElementById("modal");
const overlay = document.getElementById("overlay");
function Remove() {
  modal.classList.remove("hiden");
  overlay.classList.remove("hiden");
}
function Added() {
  modal.classList.add("hiden");
  overlay.classList.add("hiden");
}
showBtn.addEventListener("click", Remove);
closeBtn.addEventListener("click", Added);
overlay.addEventListener("click", Added);
document.addEventListener("keydown", (e) => {
  if (e.key == "Escape") {
    Added();
  }
});
