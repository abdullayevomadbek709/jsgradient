const singupForm = document.querySelector("#signup-form");

// const username = document.querySelector("#username");
const message = document.querySelector(".message");

const regEX = /^[a-zA-Z0-9]{6,12}$/;

singupForm.addEventListener("submit", (e) => {
  e.preventDefault();
  console.log("ok");

  const inputVal = singupForm.username.value;

  if (regEX.test(inputVal)) {
    message.textContent = "Malumot togri kiritildi";
  } else {
    message.textContent = "Kiritilayotgan malumot 6 va 12 orasida bo'lishi kerak. Shuningdek kichik va katta lotin hariflaridan iborat bo'lishi zarur. raqamlar ham ishtirok etishi mumkun";
  }
});

// const ism = "Omadk";
// const regEX = /^[a-zA-Z0-9]{6,12}$/
// const result = regEX.test(ism);
// console.log(result);
// const ism = "@s!Omasdk";
// const regEX = /[a-zA-Z0-9]{6,12}/;
// const result = ism.search(regEX);
// console.log(result);
singupForm.username.addEventListener('keyup',(e)=>{
    if (regEX.test(e.target.value)) {
        singupForm.username.setAttribute('class','success')
      } else {
        singupForm.username.setAttribute('class','error')
      }
})