/* dom selectors */

// 1) getElementsByTagName
// const list-item = document.getElementsByTagName('li')
// list-item.forEach((li) => {
//  console.log(li)
// })
// forEach() => bolmaydi

// 2)getElementsByClassName
// const listItem = document.getElementsByClassName('list-item')
// listItem.forEach((li) => {
//  console.log(li)
// })
// forEach() => bolmaydi

// 3) getElementsById
// const click-btn = document.getElementById('click-btn');
// console.log(click-btn);

// 4) querySelector / querySelectorAll\ Node.list qaytaradi / Nide.list forEach va boshqa metodlar ishlaydi
// const listItem = document.querySelectorAll('.list-item'); //class orqali chaqairsh
// const title = document.querySelectorAll('#title');// id orqali chaqirish
// listItem.forEach((li) => {
// console.log(li);
// });

/* text.Content/innerHTML */

// textContent
// const title =document.getElementById('title');
// title.textContent = " new text "
// const listItem = document.querySelectorAll(".list-item");
// console.log(listItem);
// listItem.forEach((item) => {
// 	console.log(item);
// 	item.textContent += " darslari";
// });

// innerHTML
// const title = document.getElementById("title");
// title.innerHTML = " <i> new text </i> ";
// const names = ["ahror", "sardor", "abbos"];

// const ol = document.querySelector("ol");

// names.forEach((name) => {
// 	ol.innerHTML += `<li> ${name}</li>`;

// });
